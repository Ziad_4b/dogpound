package com.company;
/**
 Yorkshire.java
 A class derived from Dog that holds information about
 a Yorkshire terrier. Overrides Dog speak method.
 @ TODO this file is largely incomplete

 */

public class Yorkshire extends Dog
{
    protected String name;

    public static int breedWeight= 75;
    public Yorkshire(String name)
    {
        super(name);
        this.name=name;

    }

    /**
     * Small bark -- overrides speak method in Dog
     * @return a small bask string
     */

    public String speak()
    {
        return "boof";
    }

    public static int avgBreedWeight()
    {
        return breedWeight;
    }

    @Override
    public String toString(){
        return "Name: "+name+" Height: "+breedWeight;
    }
}

