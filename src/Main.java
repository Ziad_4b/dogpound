package com.company;

public class Main {

    public static void main(String[] args)
    {
        Dog dog = new Dog("Spike");
        System.out.println("Name: " +dog.getName() + " Says: " + dog.speak());
        System.out.println();
        Labrador labrador=new Labrador("Bibi","brown" );
        System.out.println(labrador);
        System.out.println("Says: " +  labrador.speak());
        System.out.println();
        Yorkshire yorkshire=new Yorkshire("Tatu");
        System.out.println(yorkshire);
        System.out.println("Says: " +yorkshire.speak());
    }
}